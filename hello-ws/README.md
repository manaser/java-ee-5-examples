# Hello WS

## Build
```bash
mvn clean install
```
## Installation
Note: deployed on Weblogic.
Deploy the file on the application server as "an application".

## Testing
In weblogic console you can go to < myDomain >/Deployments, find hello-ws-1,
expand it, click on HelloWSService.
Click on Testing tab.
Expand HelloWSService.
Click on Test client.
Test ... 
   


