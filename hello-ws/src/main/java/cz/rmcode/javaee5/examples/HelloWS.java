package cz.rmcode.javaee5.examples;


import javax.jws.WebMethod;
import javax.jws.WebService;


@WebService
public class HelloWS {

    @WebMethod
    public String hello(final String name) {
        return "Hi " + name;
    }

}
