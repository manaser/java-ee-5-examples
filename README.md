# java EE 5 examples

This project contains simple examples how to solve implementation of various 
use-cases in java EE 5. Even Java EE is very old technology, there are still 
a lot of companies using it.
The use-cases are deployed/tested on WebLogic Server Version: 10.3.6.0 (11g).

